extern "C" %{
/*
 *  Copyright (c) 2010-2013
 *
 *  The University of Tennessee and The University
 *  of Tennessee Research Foundation.  All rights
 *  reserved.
 *
 * @precisions normal z -> s d c
 *
 */
#include "dplasma/lib/dplasmajdf.h"
#include "data_dist/matrix/matrix.h"

#if defined(HAVE_CUDA)
#include <dague/devices/cuda/dev_cuda.h>
#include "dplasma/cores/cuda_zgemm.h"
extern int *gpu_counter;
#endif  /* defined(HAVE_CUDA) */
%}

/*
 * Globals
 */
transA [type = int]
transB [type = int]

alpha  [type = dague_complex64_t]
beta   [type = dague_complex64_t]

dataA  [type = "dague_ddesc_t *"]
descA  [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataA)"]
dataB  [type = "dague_ddesc_t *"]
descB  [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataB)"]
dataC  [type = "dague_ddesc_t *"]
descC  [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataC)"]

/**************************************************
 *                       READ_A                   *
 **************************************************/
READ_A(m, k)  [profile = off]

m = 0 .. descA.mt-1
k = 0 .. descA.nt-1

: dataA(m, k)

READ A <- dataA(m, k)
       -> A GEMM(m, 0, k)
BODY
{
    printlog("rank %u <- A(%d,%d)\n", __dague_handle->super.dataA->myrank, m, k);
}
END

/**************************************************
 *                       READ_B                   *
 **************************************************/
READ_B(k, n)  [profile = off]

k = 0 .. descB.mt-1
n = 0 .. descB.nt-1

: dataB(k, n)

READ B <- dataB(k, n)
       -> B GEMM(0, n, k)

BODY
{
    printlog("rank %u <- B(%d,%d)\n", __dague_handle->super.dataB->myrank, k, n);
}
END

/**************************************************
 *                       GEMM                     *
 **************************************************/
GEMM(m, n, k)

// Execution space
m = 0 .. descC.mt-1
n = 0 .. descC.nt-1
k = 0 .. descA.nt-1

// Parallel partitioning
: dataC(m, n)

// Parameters
READ A <- (n == 0)            ? A READ_A(m, k) : A GEMM( m, (n+descC.nt-1) % descC.nt, k )
       -> (n <= (descC.nt-2)) ? A GEMM( m, (n+1) % descC.nt, k )
READ B <- (m == 0)            ? B READ_B(k, n) : B GEMM( (m+descC.mt-1) % descC.mt, n, k )
       -> (m <= (descC.mt-2)) ? B GEMM( (m+1) % descC.mt, n, k )
RW   C <- (k == descA.nt-1) ? dataC(m, n) : C GEMM( m, n, k+1 )
       -> (k == 0         ) ? dataC(m, n) : C GEMM( m, n, k-1 )

BODY [type=CUDA dyld=cublasZgemm]
{
    dague_complex64_t lbeta = (k == descA.nt-1) ? beta : (dague_complex64_t)1.0;
    int tempmm = m == descC.mt-1 ? descC.m - m * descC.mb : descC.mb;
    int tempnn = n == descC.nt-1 ? descC.n - n * descC.nb : descC.nb;
    int tempkk = k == descA.nt-1 ? descA.n - k * descA.nb : descA.nb;
    int ldam = BLKLDD(descA, m);
    int ldbk = BLKLDD(descB, k);
    int ldcm = BLKLDD(descC, m);

    return gpu_zgemm(context, this_task,
                     ( k == descA.nt-1 ), descA.nt-k,
                     transA, transB,
                     tempmm, tempnn, tempkk,
                     alpha, ldam,
                     ldbk,
                     lbeta, ldcm);
}
END

BODY
{
    dague_complex64_t lbeta = (k == descA.nt-1) ? beta : (dague_complex64_t)1.0;
    int tempmm = m == descC.mt-1 ? descC.m - m * descC.mb : descC.mb;
    int tempnn = n == descC.nt-1 ? descC.n - n * descC.nb : descC.nb;
    int tempkk = k == descA.nt-1 ? descA.n - k * descA.nb : descA.nb;
    int ldam = BLKLDD(descA, m);
    int ldbk = BLKLDD(descB, k);
    int ldcm = BLKLDD(descC, m);

#if !defined(DAGUE_DRY_RUN)
    CORE_zgemm(transA, transB,
               tempmm, tempnn, tempkk,
               alpha, A /*A(m, k)*/, ldam,
                      B /*B(k, n)*/, ldbk,
               lbeta, C /*C(m, n)*/, ldcm);
#endif  /* !defined(DAGUE_DRY_RUN) */

    printlog("gemm( %d, %d, %d )\n"
             "    ( %s, %s, %d, %d, %d, %f, A(%d,%d), %d, B(%d,%d), %d, %f, C(%d,%d), %d)\n",
             m, n, k,
             plasma_const( transA ), plasma_const( transB ),
             tempmm, tempnn, tempkk,
             creal(alpha), m, k, ldam,
                           k, n, ldbk,
             creal(lbeta), m, n, ldcm );
}
END
