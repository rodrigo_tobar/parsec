extern "C" %{
/*
 * Copyright (c) 2010-2013 The University of Tennessee and The University
 *                         of Tennessee Research Foundation. All rights
 *                         reserved.
 * Copyright (c) 2013      Inria. All rights reserved.
 * $COPYRIGHT
 *
 *
 * @precisions normal z -> s d c
 *
 */
#include "dplasma/lib/dplasmajdf.h"
#include "data_dist/matrix/matrix.h"

#if defined(HAVE_CUDA)
#include "dague/devices/cuda/dev_cuda.h"
#include "dplasma/cores/cuda_zgemm.h"
#endif  /* defined(HAVE_CUDA) */

%}

dataA     [type = "dague_ddesc_t *"]
descA     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataA)"]

potrf_zpotrf(k)
  /* Execution Space */
  k = 0..(descA.nt-1)

  /* Locality */
  : dataA(k,k)

  RW    A    <- (k == 0) ? dataA(k, k) : B potrf_zherk(k-1, k)
             -> A potrf_ztrsm(k, (k+1)..(descA.nt-1))
             -> A trtri_ztrsmR(k, 0..(k-1))
             -> A trtri_ztrsmL(k, (k+1)..(descA.nt-1))
             -> A trtri_ztrtri(k)

; 44

BODY
{
    int tempkm = (k == (descA.nt-1)) ? (descA.n - k * descA.nb) : descA.nb;
    int ldak = BLKLDD( descA, k );
    int info = 0;

    dague_data_transfer_ownership_to_copy(gA->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);

    printlog("CORE_potrf_zpotrf(%d)\n"
             "\t(PlasmaUpper, tempkm, A(%d,%d)[%p], ldak, descA.nb*k)\n",
             k, k, k, A);

#if !defined(DAGUE_DRY_RUN)
    CORE_zpotrf(PlasmaUpper, tempkm,
                A /* dataA(k,k) */, ldak, &info );
#endif  /* !defined(DAGUE_DRY_RUN) */
}
END

potrf_ztrsm(k, m)
  /* Execution Space */
  k = 0    ..(descA.nt-2)
  m = (k+1)..(descA.nt-1)

  /* Locality */
  : dataA(k,m)

  READ  A    <- A potrf_zpotrf(k)
  RW    B    <- (k == 0) ? dataA(k, m) : C potrf_zgemm(k-1, m, k)
             -> A potrf_zherk(k, m)
             -> A potrf_zgemm(k, (m+1)..(descA.nt-1), m)
             -> B potrf_zgemm(k, m, (k+1)..(m-1))
             -> B trtri_ztrsmL(k, m)

; 40

BODY
{
    int tempmm = (m == (descA.nt-1)) ? (descA.n - m * descA.nb) : descA.nb;
    int ldak = BLKLDD( descA, k );

    dague_data_transfer_ownership_to_copy(gB->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);

    printlog("CORE_potrf_ztrsm(%d, %d)\n"
             "\t(PlasmaLeft, PlasmaUpper, PlasmaConjTrans, PlasmaNonUnit, descA.nb, tempmm, 1., A(%d,%d)[%p], ldak, A(%d,%d)[%p], ldak)\n",
             k, m, k, k, A, k, m, B);

#if !defined(DAGUE_DRY_RUN)
    CORE_ztrsm(PlasmaLeft, PlasmaUpper, PlasmaConjTrans, PlasmaNonUnit,
               descA.nb, tempmm,
               1., A /* dataA(k,k) */, ldak,
                   B /* dataA(k,m) */, ldak );
#endif  /* !defined(DAGUE_DRY_RUN) */
}
END

potrf_zherk(k, m)
  /* Execution Space */
  k = 0    ..(descA.nt-2)
  m = (k+1)..(descA.nt-1)

  /* Locality */
  : dataA(m,m)

  READ  A    <- B potrf_ztrsm(k, m)
  RW    B    <- (k == 0) ? dataA(m, m) : B potrf_zherk(k-1, m)
             -> (m == (k+1)) ? A potrf_zpotrf(m) : B potrf_zherk(k+1, m)

  CTL   ctl0 -> ctl0 trtri_ztrsmL(k, m)

; 40

BODY
{
    int tempmm = (m == (descA.nt-1)) ? (descA.n - m * descA.nb) : descA.nb;
    int ldak = BLKLDD( descA, k );
    int ldam = BLKLDD( descA, m );

    dague_data_transfer_ownership_to_copy(gB->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);

    printlog("CORE_potrf_zherk(%d, %d)\n"
             "\t(PlasmaUpper, PlasmaConjTrans, tempmm, descA.mb, -1., A(%d,%d)[%p], ldak, 1., A(%d,%d)[%p], ldam)\n",
             k, m, k, m, A, m, m, B);
#if !defined(DAGUE_DRY_RUN)
    CORE_zherk(PlasmaUpper, PlasmaConjTrans,
               tempmm, descA.mb,
               -1., A /* dataA(k,m) */, ldak,
                1., B /* dataA(m,m) */, ldam );
#endif  /* !defined(DAGUE_DRY_RUN) */
}
END

potrf_zgemm(k, m, n)
  /* Execution Space */
  k = 0    ..(descA.nt-2)
  m = (k+2)..(descA.nt-1)
  n = (k+1)..(m-1)

  /* Locality */
  : dataA(n,m)

  READ  A    <- B potrf_ztrsm(k, n)
  READ  B    <- B potrf_ztrsm(k, m)
  RW    C    <- (k == 0) ? dataA(n, m) : C potrf_zgemm(k-1, m, n)
             -> (n == (k+1)) ? B potrf_ztrsm(n, m) : C potrf_zgemm(k+1, m, n)

  CTL   ctl1 -> ctl1 trtri_ztrsmL(k, m)
  CTL   ctl2 -> ctl2 trtri_ztrsmL(k, n)

; 40

BODY [type=CUDA dyld=cublasZgemm]
{
    int tempmm = (m == (descA.nt-1)) ? (descA.n - m * descA.nb) : descA.nb;
    int ldak = BLKLDD( descA, k );
    int ldan = BLKLDD( descA, n );

    return gpu_zgemm(context, this_task,
                     ( n == k+1 ), (n-k),
                     PlasmaConjTrans, PlasmaNoTrans,
                     descA.mb, tempmm, descA.mb,
                     (dague_complex64_t)-1., ldak,
                                             ldak,
                     (dague_complex64_t) 1., ldan);
}
END

BODY
{
    int tempmm = (m == (descA.nt-1)) ? (descA.n - m * descA.nb) : descA.nb;
    int ldak = BLKLDD( descA, k );
    int ldan = BLKLDD( descA, n );

    printlog("CORE_potrf_zgemm(%d, %d, %d)\n"
             "\t(PlasmaConjTrans, PlasmaNoTrans, descA.mb, tempmm, descA.mb, -1, A(%d,%d)[%p], ldak, A(%d,%d)[%p], ldak, 1., A(%d,%d)[%p], descA.mb)\n",
             k, m, n, k, n, A, k, m, B, n, m, C);

#if !defined(DAGUE_DRY_RUN)
    CORE_zgemm(PlasmaConjTrans, PlasmaNoTrans,
               descA.mb, tempmm, descA.mb,
               -1., A /* dataA(k,n) */, ldak,
                    B /* dataA(k,m) */, ldak,
                1., C /* dataA(n,m) */, ldan );
#endif  /* !defined(DAGUE_DRY_RUN) */
}
END

trtri_ztrsmL(k, n)
  /* Execution Space */
  k = 0    ..(descA.nt-2)
  n = (k+1)..(descA.nt-1)

  /* Locality */
  : dataA(k,n)

  READ  A    <- A potrf_zpotrf(k)
  RW    B    <- B potrf_ztrsm(k, n)
             -> (n == (k+1)) ? A trtri_zgemm(n, n-1, (n+1)..(descA.nt-1))
             -> (n == (k+1)) ? B trtri_ztrsmR(n, n-1)
             -> (n >  (k+1)) ? C trtri_zgemm(k+1, k, n)
             -> B trtri_zgemm(k, 0..(k-1), n)

  CTL   ctl0 <- ctl0 potrf_zherk(k, n)
  CTL   ctl1 <- ctl1 potrf_zgemm(k, (n+1)..(descA.nt-1), n)
  CTL   ctl2 <- ctl2 potrf_zgemm(k, n, (k+1)..(n-1))
  CTL   ctl3 -> (k == 0) ? ctl3 trtri_ztrtri(k)

; 30

BODY
{
    int tempkm = (k == (descA.mt-1)) ? (descA.m - k * descA.mb) : descA.mb;
    int tempnn = (n == (descA.nt-1)) ? (descA.n - n * descA.nb) : descA.nb;
    int ldak = BLKLDD( descA, k );

    dague_data_transfer_ownership_to_copy(gB->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);

    printlog("CORE_trtri_ztrsmL(%d, %d)\n"
             "\t(PlasmaLeft, PlasmaUpper, PlasmaNoTrans, PlasmaNonUnit, tempkm, tempnn, -1, A(%d,%d)[%p], ldak, A(%d,%d)[%p], ldak)\n",
             k, n, k, k, A, k, n, B);

#if !defined(DAGUE_DRY_RUN)
    CORE_ztrsm(PlasmaLeft, PlasmaUpper, PlasmaNoTrans, PlasmaNonUnit,
               tempkm, tempnn,
               -1., A /* dataA(k,k) */, ldak,
                    B /* dataA(k,n) */, ldak );
#endif  /* !defined(DAGUE_DRY_RUN) */
}
END

trtri_zgemm(k, m, n)
  /* Execution Space */
  k = 1    ..(descA.nt-2)
  m = 0    ..(k-1)
  n = (k+1)..(descA.nt-1)

  /* Locality */
  : dataA(m,n)

  READ  A    <- (k == (m+1)) ? B trtri_ztrsmL(m+1-1, m+1) : C trtri_zgemm(k-1, m, k)
  READ  B    <- B trtri_ztrsmL(k, n)
  RW    C    <- (k == (m+1)) ? B trtri_ztrsmL(m, n) : C trtri_zgemm(k-1, m, n)
             -> (n == (k+1)) ? B trtri_ztrsmR(n, m) : C trtri_zgemm(k+1, m, n)
             -> (n == (k+1)) ? A trtri_zgemm(n, m, (n+1)..(descA.nt-1))

  CTL   ctl4 -> ctl4 trtri_ztrsmR(k, m)
  CTL   ctl5 <- ctl5 trtri_zgemm(m, 0..(m-1), n)
             -> ctl5 trtri_zgemm((k+1)..(n-1), k, n)
  CTL   ctl6 -> (n == (k+1)) ? ctl6 trtri_ztrsmR(k+1, k)

; 30

BODY [type=CUDA dyld=cublasZgemm]
{
    int tempnn = (n == (descA.nt-1)) ? (descA.n - n * descA.nb) : descA.nb;
    int tempkm = (k == (descA.mt-1)) ? (descA.m - k * descA.mb) : descA.mb;
    int ldam = BLKLDD( descA, m );
    int ldak = BLKLDD( descA, k );

    return gpu_zgemm(context, this_task,
                     ( n == k+1 ), (n-k),
                     PlasmaNoTrans, PlasmaNoTrans,
                     descA.mb, tempnn, tempkm,
                     (dague_complex64_t)1., ldam,
                                            ldak,
                     (dague_complex64_t)1., ldam);
}
END

BODY
{
    int tempnn = (n == (descA.nt-1)) ? (descA.n - n * descA.nb) : descA.nb;
    int tempkm = (k == (descA.mt-1)) ? (descA.m - k * descA.mb) : descA.mb;
    int ldam = BLKLDD( descA, m );
    int ldak = BLKLDD( descA, k );

    printlog("CORE_trtri_zgemm(%d, %d, %d)\n"
             "\t(PlasmaNoTrans, PlasmaNoTrans, descA.mb, tempnn, tempkm, 1., A(%d,%d)[%p], ldam, A(%d,%d)[%p], ldak, 1., A(%d,%d)[%p], ldam)\n",
             k, m, n, m, k, A, k, n, B, m, n, C);

#if !defined(DAGUE_DRY_RUN)
    CORE_zgemm(PlasmaNoTrans, PlasmaNoTrans,
               descA.mb, tempnn, tempkm,
               1., A /* dataA(m,k) */, ldam,
                   B /* dataA(k,n) */, ldak,
               1., C /* dataA(m,n) */, ldam );
#endif  /* !defined(DAGUE_DRY_RUN) */
}
END

trtri_ztrsmR(k, m)
  /* Execution Space */
  k = 1..(descA.mt-1)
  m = 0..(k-1)

  /* Locality */
  : dataA(m,k)

  READ  A    <- A potrf_zpotrf(k)
  RW    B    <- (k == (m+1)) ? B trtri_ztrsmL(k-1, k) : C trtri_zgemm(k-1, m, k)
             -> A lauum_zherk(k, m)
             -> B lauum_ztrmm(k, m)
             -> A lauum_zgemm(k, m, (m+1)..(k-1))
             -> B lauum_zgemm(k, 0..(m-1), m)

  CTL   ctl4 <- ctl4 trtri_zgemm(k, m, (k+1)..(descA.nt-1))
  CTL   ctl6 <- (k == (m+1)) ? ctl6 trtri_zgemm(k-1, 0..(k-2), k)
  CTL   ctl7 -> ctl7 trtri_ztrtri(k)

; 30

BODY
{
    int tempkm = (k == (descA.mt-1)) ? (descA.m - k * descA.mb) : descA.mb;
    int ldak = BLKLDD( descA, k );
    int ldam = BLKLDD( descA, m );

    dague_data_transfer_ownership_to_copy(gB->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);

    printlog("CORE_trtri_ztrsmR(%d, %d)\n"
             "\t(PlasmaRight, PlasmaUpper, PlasmaNoTrans, PlasmaNonUnit, descA.mb, tempkm, 1., A(%d,%d)[%p], ldak, A(%d,%d)[%p], ldam)\n",
             k, m, k, k, A, m, k, B);
#if !defined(DAGUE_DRY_RUN)
    CORE_ztrsm(PlasmaRight, PlasmaUpper, PlasmaNoTrans, PlasmaNonUnit,
               descA.mb, tempkm,
               1., A /* dataA(k,k) */, ldak,
                   B /* dataA(m,k) */, ldam );
#endif  /* !defined(DAGUE_DRY_RUN) */
}
END

trtri_ztrtri(k)
  /* Execution Space */
  k = 0..(descA.mt-1)

  /* Locality */
  : dataA(k,k)

  RW    A    <- A potrf_zpotrf(k)
             -> A lauum_zlauum(k)
             -> A lauum_ztrmm(k, 0..(k-1))

  CTL   ctl3 <- (k == 0) ? ctl3 trtri_ztrsmL(0, 1..(descA.nt-1))
  CTL   ctl7 <- (k >  0) ? ctl7 trtri_ztrsmR(k, 0..(k-1))

; 30

BODY
{
    int tempkm = (k == (descA.mt-1)) ? (descA.m - k * descA.mb) : descA.mb;
    int ldak = BLKLDD( descA, k );
    int info = 0;

    dague_data_transfer_ownership_to_copy(gA->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);

    printlog("CORE_trtri_ztrtri(%d)\n"
             "\t(PlasmaUpper, PlasmaNonUnit, tempkm, A(%d,%d)[%p], ldak, descA.mb*k)\n",
             k, k, k, A);

#if !defined(DAGUE_DRY_RUN)
    CORE_ztrtri(PlasmaUpper, PlasmaNonUnit, tempkm,
                A /* dataA(k,k) */, ldak, &info );
#endif  /* !defined(DAGUE_DRY_RUN) */
}
END

lauum_zherk(k, m)
  /* Execution Space */
  k = 1..(descA.mt-1)
  m = 0..(k-1)

  /* Locality */
  : dataA(m,m)

  READ  A    <- B trtri_ztrsmR(k, m)
  RW    B    <- (k == (m+1)) ? A lauum_zlauum(k-1) : B lauum_zherk(k-1, m)
             -> (k <  (descA.nt-1)) ? B lauum_zherk(k+1, m)
             -> (k == (descA.nt-1)) ? dataA(m, m)

  CTL   ctl8 -> ctl8 lauum_ztrmm(k, m)

; 20

BODY
{
    int tempkn = (k == (descA.nt-1)) ? (descA.n - k * descA.nb) : descA.nb;
    int ldam = BLKLDD( descA, m );

    dague_data_transfer_ownership_to_copy(gB->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);

    printlog("CORE_lauum_zherk(%d, %d)\n"
             "\t(PlasmaUpper, PlasmaNoTrans, descA.mb, tempkn, 1., A(%d,%d)[%p], ldam, 1., A(%d,%d)[%p], ldam)\n",
             k, m, m, k, A, m, m, B);

#if !defined(DAGUE_DRY_RUN)
    CORE_zherk(PlasmaUpper, PlasmaNoTrans,
               descA.mb, tempkn,
               1., A /* dataA(m,k) */, ldam,
               1., B /* dataA(m,m) */, ldam );
#endif  /* !defined(DAGUE_DRY_RUN) */
}
END

lauum_zgemm(k, m, n)
  /* Execution Space */
  k = 2    ..(descA.mt-1)
  m = 0    ..(k-2)
  n = (m+1)..(k-1)

  /* Locality */
  : dataA(m,n)

  READ  A    <- B trtri_ztrsmR(k, m)
  READ  B    <- B trtri_ztrsmR(k, n)
  RW    C    <- (k == (n+1)) ? B lauum_ztrmm(k-1, m) : C lauum_zgemm(k-1, m, n)
             -> (k == (descA.nt-1)) ? dataA(m, n)
             -> (k <  (descA.nt-1)) ? C lauum_zgemm(k+1, m, n)

  CTL   ctl9  -> ctl9  lauum_ztrmm(k, m)
  CTL   ctl10 -> ctl10 lauum_ztrmm(k, n)

; 20

BODY [type=CUDA dyld=cublasZgemm]
{
    int tempkn = (k == (descA.nt-1)) ? (descA.n - k * descA.nb) : descA.nb;
    int ldam = BLKLDD( descA, m );
    int ldan = BLKLDD( descA, n );

    return gpu_zgemm(context, this_task,
                     descA.nt == (k+1), descA.nt-k,
                     PlasmaNoTrans, PlasmaConjTrans,
                     descA.mb, descA.nb, tempkn,
                     (dague_complex64_t)1., ldam,
                                            ldan,
                     (dague_complex64_t)1., ldam);
}
END

BODY
{
    int tempkn = (k == (descA.nt-1)) ? (descA.n - k * descA.nb) : descA.nb;
    int ldam = BLKLDD( descA, m );
    int ldan = BLKLDD( descA, n );

    printlog("CORE_lauum_zgemm(%d, %d, %d)\n"
             "\t(PlasmaNoTrans, PlasmaConjTrans, descA.mb, descA.nb, tempkn, 1., A(%d,%d)[%p], ldam, A(%d,%d)[%p], ldan, 1., A(%d,%d)[%p], ldam)\n",
             k, m, n, m, k, A, n, k, B, m, n, C);

#if !defined(DAGUE_DRY_RUN)
    CORE_zgemm(PlasmaNoTrans, PlasmaConjTrans,
               descA.mb, descA.nb, tempkn,
               1., A /* dataA(m,k) */, ldam,
                   B /* dataA(n,k) */, ldan,
               1., C /* dataA(m,n) */, ldam );
#endif  /* !defined(DAGUE_DRY_RUN) */
}
END

lauum_ztrmm(k, m)
  /* Execution Space */
  k = 1..(descA.mt-1)
  m = 0..(k-1)

  /* Locality */
  : dataA(m,k)

  READ  A    <- A trtri_ztrtri(k)
  RW    B    <- B trtri_ztrsmR(k, m)
             -> (k == (descA.nt-1)) ? dataA(m, k)
             -> (k <  (descA.nt-1)) ? C lauum_zgemm(k+1, m, k)

  CTL   ctl8  <- ctl8  lauum_zherk(k, m)
  CTL   ctl9  <- ctl9  lauum_zgemm(k, m, (m+1)..(k-1))
  CTL   ctl10 <- ctl10 lauum_zgemm(k, 0..(m-1), m)
  CTL   ctl11 -> ctl11 lauum_zlauum(k)

; 20

BODY
{
    int tempkn = (k == (descA.nt-1)) ? (descA.n - k * descA.nb) : descA.nb;
    int ldak = BLKLDD( descA, k );
    int ldam = BLKLDD( descA, m );

    dague_data_transfer_ownership_to_copy(gB->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);

    printlog("CORE_lauum_ztrmm(%d, %d)\n"
             "\t(PlasmaRight, PlasmaUpper, PlasmaConjTrans, PlasmaNonUnit, descA.mb, tempkn, 1., A(%d,%d)[%p], ldak, A(%d,%d)[%p], ldam)\n",
             k, m, k, k, A, m, k, B);

#if !defined(DAGUE_DRY_RUN)
    CORE_ztrmm(PlasmaRight, PlasmaUpper, PlasmaConjTrans, PlasmaNonUnit,
               descA.mb, tempkn,
               1., A /* dataA(k,k) */, ldak,
                   B /* dataA(m,k) */, ldam );
#endif  /* !defined(DAGUE_DRY_RUN) */
}
END

lauum_zlauum(k)
  /* Execution Space */
  k = 0..(descA.mt-1)

  /* Locality */
  : dataA(k,k)

  RW    A    <- A trtri_ztrtri(k)
             -> (k == (descA.nt-1)) ? dataA(k, k)
             -> (k <  (descA.nt-1)) ? B lauum_zherk(k+1, k)

  CTL   ctl11 <- ctl11 lauum_ztrmm(k, 0..(k-1))

; 20

BODY
{
    int tempkn = (k == (descA.nt-1)) ? (descA.n - k * descA.nb) : descA.nb;
    int ldak = BLKLDD( descA, k );

    dague_data_transfer_ownership_to_copy(gA->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);

    printlog("CORE_lauum_zlauum(%d)\n"
             "\t(PlasmaUpper, tempkn, A(%d,%d)[%p], ldak)\n",
             k, k, k, A);

#if !defined(DAGUE_DRY_RUN)
    CORE_zlauum(PlasmaUpper, tempkn,
                A /* dataA(k,k) */, ldak );
#endif  /* !defined(DAGUE_DRY_RUN) */
}
END

