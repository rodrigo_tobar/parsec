extern "C" %{
/*
 * Copyright (c) 2010-2013 The University of Tennessee and The University
 *                         of Tennessee Research Foundation. All rights
 *                         reserved.
 * Copyright (c) 2013      Inria. All rights reserved.
 * $COPYRIGHT
 *
 *
 * @precisions normal z -> s d c
 *
 * 4 versions: LN, LC, RN, RC
 *     - When N <-> C, only arrows on A are swap.
 *     - When L <-> R, n becomes m, and respectively m becomes n
 *
 */
#include "dplasma/lib/dplasmajdf.h"
#include "data_dist/matrix/matrix.h"

%}

side      [type = "PLASMA_enum"]
trans     [type = "PLASMA_enum"]
dataA     [type = "dague_ddesc_t *"]
dataB     [type = "dague_ddesc_t *"]
dataTS    [type = "dague_ddesc_t *" aligned=dataA ]
dataTT    [type = "dague_ddesc_t *" aligned=dataA ]
qrtree    [type = "dplasma_qrtree_t"]

descA   [type = "tiled_matrix_desc_t" default="*((tiled_matrix_desc_t*)dataA)"  hidden=on]
descB   [type = "tiled_matrix_desc_t" default="*((tiled_matrix_desc_t*)dataB)"  hidden=on]
descTS  [type = "tiled_matrix_desc_t" default="*((tiled_matrix_desc_t*)dataTS)" hidden=on]
descTT  [type = "tiled_matrix_desc_t" default="*((tiled_matrix_desc_t*)dataTT)" hidden=on]
ib      [type = "int" hidden = on default = "descTS.mb" ]
KT      [type = "int" hidden = on default = "descA.mt-1" ]
KT2     [type = "int" hidden = on default = "dplasma_imin( KT, descA.nt-2 )" ]
p_work  [type = "dague_memory_pool_t *" size = "(sizeof(PLASMA_Complex64_t)*ib*(descTS.nb))"]

zunmlq(k, i, n)
  /* Execution space */
  k = 0 .. KT
  i = 0 .. inline_c %{ return qrtree.getnbgeqrf( &qrtree, k ) - 1; %}
  n = 0 .. descB.nt-1

  p     = inline_c %{ return qrtree.getm(    &qrtree, k, i); %}
  nextp = inline_c %{ return qrtree.nextpiv( &qrtree, k, p, descA.nt); %}

  /* Locality */
  : dataB(p, n)

  READ  A    <- A zunmlq_in(k, i)   [type = UPPER_TILE]
  READ  T    <- T zunmlq_in(k, i)   [type = LITTLE_T]
  RW    C    -> ( k == 0 ) ? dataB(p, n)
             -> ( k >  0 ) ? A2 zttmlq(k-1, p, n)
             <- ( k == descA.nt-1 ) ? dataB(p, n)
             <- ((k <  descA.nt-1) & (nextp != descA.nt) ) ? A1 zttmlq(k, nextp, n)
             <- ((k <  descA.nt-1) & (nextp == descA.nt) ) ? A2 zttmlq(k, p,     n)

BODY
{
    int temppm = (p == (descB.mt-1)) ? (descB.m - p * descB.mb) : descB.mb;
    int tempnn = (n == (descB.nt-1)) ? (descB.n - n * descB.nb) : descB.nb;
    int tempkm = (k == (descA.mt-1)) ? (descA.m - k * descA.mb) : descA.mb;
    int tempmin = dplasma_imin( temppm, tempkm );
    int ldak    = BLKLDD( descA, k );
    int ldbp    = BLKLDD( descB, p );

    void *W = dague_private_memory_pop( p_work );

    printlog("CORE_zunmlq(%d, %d, %d) [%d, %d]\n"
             "\t(side=%s, trans=%s, M=%d, N=%d, K=%d, ib=%d,\n"
             "\t A(%d,%d)[%p], lda=%d, T(%d,%d)[%p], ldt=%d, B(%d,%d)[%p], ldb=%d, W=%p, LDW=%d)\n",
             k, i, n, p, nextp, plasma_const( side ), plasma_const( trans ), temppm, tempnn, tempmin, ib,
             k, p, A, ldak, k, p, T, descTS.mb, p, n, C, ldbp, W, descTS.nb );

#if !defined(DAGUE_DRY_RUN)
    CORE_zunmlq(
        side, trans,
        temppm, tempnn, tempmin, ib,
        A /* A(k, p) */, ldak,
        T /* T(k, p) */, descTS.mb,
        C /* B(p, n) */, ldbp,
        W, descTS.nb );
#endif /* !defined(DAGUE_DRY_RUN) */

    dague_private_memory_push( p_work, W );
}
END

zunmlq_in(k,i)  [profile = off]
  k = 0 .. KT
  i = 0 .. inline_c %{ return qrtree.getnbgeqrf( &qrtree, k ) - 1; %}
  p = inline_c %{ return qrtree.getm( &qrtree, k, i); %}

  : dataA(k, p)

  RW A <- dataA(k, p)                      [type = UPPER_TILE]
       -> A zunmlq(k, i, 0 .. descB.nt-1)  [type = UPPER_TILE]

  RW T <- dataTS(k, p)                     [type = LITTLE_T]
       -> T zunmlq(k, i, 0 .. descB.nt-1)  [type = LITTLE_T]

BODY
{
    /* nothing */
}
END

zttmlq(k, m, n)
  /* Execution Space */
  k = 0   .. KT2
  m = k+1 .. descB.mt-1
  n = 0   .. descB.nt-1

  p =     inline_c %{ return qrtree.currpiv( &qrtree, k, m);    %}
  nextp = inline_c %{ return qrtree.nextpiv( &qrtree, k, p, m); %}
  prevp = inline_c %{ return qrtree.prevpiv( &qrtree, k, p, m); %}
  prevm = inline_c %{ return qrtree.prevpiv( &qrtree, k, m, m); %}
  type  = inline_c %{ return qrtree.gettype( &qrtree, k, m );   %}
  ip    = inline_c %{ return qrtree.geti(    &qrtree, k, p );   %}
  im    = inline_c %{ return qrtree.geti(    &qrtree, k, m );   %}

  type1 = inline_c %{ return (k == KT) ? -1 : qrtree.gettype( &qrtree, k+1, m ); %}
  im1   = inline_c %{ return (k == KT) ? -1 : qrtree.geti(    &qrtree, k+1, m ); %}

  /* Locality */
  : dataB(m, n)

  RW    A1   ->  (prevp == descA.nt) ? C zunmlq( k, ip, n ) : A1 zttmlq(k, prevp, n )

             <- ((nextp == descA.nt) & ( p == k ) ) ? A  zttmlq_out_B(p, n)
             <- ((nextp == descA.nt) & ( p != k ) ) ? A2 zttmlq( k, p,     n)
             <-  (nextp != descA.nt) ?                A1 zttmlq( k, nextp, n)

  RW    A2   -> ( (type  == 0 ) && (k     == 0        ) ) ? dataB(m, n)
             -> ( (type  == 0 ) && (k     != 0        ) ) ? A2 zttmlq(k-1, m, n )
             -> ( (type  != 0 ) && (prevm == descA.nt ) ) ? C  zunmlq(k, im, n )
             -> ( (type  != 0 ) && (prevm != descA.nt ) ) ? A1 zttmlq(k, prevm, n )

             <-   (k == KT)                    ? dataB( m, n )
             <- ( (k <  KT) && (type1 != 0 ) ) ? C  zunmlq( k+1, im1, n )
             <- ( (k <  KT) && (type1 == 0 ) ) ? A2 zttmlq( k+1, m, n )

  READ  V    <- (type == 0) ? A zttmlq_in(k,m)
             <- (type != 0) ? A zttmlq_in(k,m)                       [type = LOWER_TILE]

  READ  T    <- T zttmlq_in(k,m)                                     [type = LITTLE_T]

BODY
{
    int tempmm = ( m == (descB.mt-1)) ? (descB.m - m * descB.mb) : descB.mb;
    int tempnn = ( n == (descB.nt-1)) ? (descB.n - n * descB.nb) : descB.nb;
    int tempkm = ( k == (descA.mt-1)) ? (descA.m - k * descA.mb) : descA.mb;
    int ldak = BLKLDD( descA, k );
    int ldbp = BLKLDD( descB, p );
    int ldbm = BLKLDD( descB, m );
    int ldwork = ib;

    void *W = dague_private_memory_pop( p_work );

    printlog("CORE_ztsmlq(%d, %d, %d)\n"
             "\t(side=%s, trans=%s, M1=%d, N1=%d, M2=%d, N2=%d, K=%d, ib=%d,\n"
             "\t A1(%d,%d)[%p], lda1=%d, A2(%d,%d)[%p], lda2=%d, V(%d,%d)[%p], ldv=%d, T(%d,%d)[%p], ldt=%d, W=%p, LDW=%d)\n",
             k, m, n, plasma_const( side ), plasma_const( trans ),
             descB.mb, tempnn, tempmm, tempnn, tempkm, ib,
             p, n, A1, ldbp, m, n, A2, ldbm, k, m, V, ldak, k, m, T, descTS.mb, W, ldwork );

#if !defined(DAGUE_DRY_RUN)
    if ( type == DPLASMA_QR_KILLED_BY_TS ) {
        CORE_ztsmlq(
            side, trans,
            descB.mb, tempnn, tempmm, tempnn, tempkm, ib,
            A1 /* B(p, n) */, ldbp,
            A2 /* B(m, n) */, ldbm,
            V  /* A(k, m) */, ldak,
            T  /* T(k, m) */, descTT.mb,
            W, ldwork );
    } else {
        CORE_zttmlq(
            side, trans,
            descB.mb, tempnn, tempmm, tempnn, tempkm, ib,
            A1 /* B(p, n) */, ldbp,
            A2 /* B(m, n) */, ldbm,
            V  /* A(k, m) */, ldak,
            T  /* T(k, m) */, descTT.mb,
            W, ldwork );
    }
#endif /* !defined(DAGUE_DRY_RUN) */

    dague_private_memory_push( p_work, W );
}
END

zttmlq_in(k, n)  [profile = off]
  k = 0   .. KT2
  n = k+1 .. descA.nt-1
  type = inline_c %{ return qrtree.gettype( &qrtree, k, n );   %}

  : dataA(k, n)

  RW A <- dataA(k, n)
       -> (type == 0) ? V zttmlq(k, n, 0..descB.nt-1)
       -> (type != 0) ? V zttmlq(k, n, 0..descB.nt-1) [type = LOWER_TILE]

  RW T <- dataTT(k, n)                                [type = LITTLE_T]
       -> T zttmlq(k, n, 0..descB.nt-1)               [type = LITTLE_T]

BODY
{
    /* nothing */
}
END

zttmlq_out_B(k, n) [profile = off]
  k = 0 .. KT2
  n = 0 .. descB.nt-1
  prevp = inline_c %{ return qrtree.prevpiv( &qrtree, k, k, k ); %}

  : dataB(k, n)

  RW A -> A1 zttmlq( k, prevp, n )
       <- dataB(k, n)

BODY
{
    /* nothing */
}
END
